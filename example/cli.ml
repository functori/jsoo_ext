open Ezjs_min
open Jext

{%%template|
<div class="container w-75">
  <textarea v-model="req" rows="3" class="form-control mt-5 mb-3"></textarea>
  <div class="text-center my-2">
    <button class="btn btn-primary" @click="request()">SEND</button>
  </div>
  <div id="result" class="my-3"></div>
</div>
|}

include Client.Lib(Ttypes)

let undefined_callback cb =
  Option.map (fun cb -> (fun arg -> Unsafe.fun_call cb [| arg |])) @@ Optdef.to_option cb

let request ?id req cb =
  let callback = Option.map (fun cb ->
      (fun res -> cb (response_to_jsoo res))) cb in
  send_req ?callback ?id (Ttypes.request_of_jsoo req)

let () =
  export "jext" @@
  object%js
    method request req cb = request req (undefined_callback cb)
  end

let get_url_params () =
  let url = to_string Dom_html.window##.location##.href in
  match String.rindex_opt url '?' with
  | None -> []
  | Some i ->
    let s = String.sub url (i+1) (String.length url - i - 1) in
    List.filter_map (fun s -> match String.split_on_char '=' s with
        | [ k; v] -> Some (k, v)
        | _ -> None) @@ String.split_on_char '&' s

let%data req = ""

let%meth request app =
  let params = get_url_params () in
  let id = List.assoc_opt "id" params in
  let id = match id with
    | None -> None
    | Some id -> try Some (int_of_string id) with _ -> log "%S" id; None in
  request ?id (_JSON##parse app##.req) @@ Some (fun res ->
      let elt = Ezjs_dom.by_id "result" in
      let cs = Unsafe.global##._JSONFormatter in
      let formatter = new%js cs (Unsafe.inject res) 3 in
      let html = formatter##render in
      Ezjs_dom.(replaceChildren elt [ html ]))

[%%app {conv; mount}]
