open Ezjs_min
module Types = Types

let iter n f =
  let rec aux = function
    | i when i < 0 -> ()
    | i -> f (n-i); aux (i-1) in
  aux (n-1)

let fold n acc f =
  let rec aux acc = function
    | i when i < 0 -> acc
    | i ->
      let acc = f acc (n-i) in
      aux acc (i-1) in
  aux acc (n-1)

let encode x =
  let s = _JSON##stringify x in
  (* let a16 = new%js Typed_array.uint16Array s##.length in
   * iter a16##.length (fun i -> Typed_array.set a16 i (int_of_float @@ s##charCodeAt i));
   * let a8 = new%js Typed_array.uint8Array_fromBuffer a16##.buffer in
   * let s = fold a8##.byteLength (string "") (fun s i ->
   *     match Optdef.to_option @@ Typed_array.get a8 i with
   *     | None -> s
   *     | Some j -> s##concat (string_constr##fromCharCode j)) in *)
  let s = to_string @@ Unsafe.global##.self##btoa s in
  let s = String.map (fun c -> if c = '/' then '_' else c) s in
  List.hd @@ String.split_on_char '=' s

let decode s =
  let s = String.map (fun c -> if c = '_' then '/' else c) s in
  let b = Unsafe.global##.self##atob (string s) in
  (* log "TES12";
   * let a8 = new%js Typed_array.uint8Array b##.length in
   * log "TES13";
   * iter a8##.length (fun i -> Typed_array.set a8 i (b##charCodeAt i));
   * log "TES14";
   * let a16 = new%js Typed_array.uint16Array_fromBuffer a8##.buffer in
   * log "TES15";
   * let s = fold a16##.byteLength (string "") (fun s i ->
   *     match Optdef.to_option @@ Typed_array.get a16 i with
   *     | None -> s
   *     | Some j -> s##concat (string_constr##fromCharCode j)) in
   * log "TES16"; *)
  _JSON##parse b
