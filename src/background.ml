open Ezjs_min
open Chrome
open Common.Types

let print_conn (port : Utils.Runtime.port t) =
  match Optdef.to_option port##.sender with
  | None -> ()
  | Some sender ->
    match Optdef.to_option sender##.url with
    | None -> log_str "connection received"
    | Some url -> log "%s connected" (to_string url)

let load_config = function
  | None -> ()
  | Some (filename, f) ->
    let config_url = Runtime.getURL filename in
    EzReq.get (EzAPI.URL config_url) f

let port_table : (int, Utils.Runtime.port t * request_source) Hashtbl.t = Hashtbl.create 512

let add_port ~id ~src (port : Utils.Runtime.port t) =
  match Hashtbl.find_opt port_table id with
  | Some (_, src) -> src
  | None -> Hashtbl.add port_table id (port, src); src

let get_port ~id =
  Hashtbl.find_opt port_table id

let remove_port ~port =
  Hashtbl.iter (fun id (p, _) -> if p = port then Hashtbl.remove port_table id) port_table

module type S = sig
  include S
  val handle_config : (string * (string -> unit)) option
  val handle_request :
    src:request_source -> id:int -> request ->
    ((response_ok option, response_error) result -> unit) -> unit
end

module Lib(S : S) = struct
  include Make(S)

  let send_res ?(ok=true) ~id ?port res_output =
    match get_port ~id, port with
    | None, None -> ()
    | Some (port, _), _ | _, Some port ->
      let res = response_aux_to_jsoo response_jsoo_conv
          {res_output; res_id=id; res_src=`background; res_ok=ok} in
      port##postMessage res

  let main () =
    load_config S.handle_config;
    Runtime.onConnect @@ fun port ->
    print_conn port;
    Utils.Browser.addListener1 (port##.onDisconnect) (fun port -> remove_port ~port);
    Utils.Browser.addListener1 (port##.onMessage) @@ fun req ->
    try
      let req = request_aux_of_jsoo S.request_jsoo_conv req in
      let id, src = req.req_id, req.req_src in
      let src = add_port ~id ~src port in
      try
        S.handle_request ~id ~src req.req_input @@ function
        | Ok None -> ()
        | Ok (Some r) -> send_res ~id (Ok r)
        | Error e -> send_res ~id ~ok:false (Error (`custom e))
      with exn ->
        send_res ~id ~ok:false
          (Error (`generic ("extension error", Printexc.to_string exn)))
    with exn ->
      send_res ~id:req##.id ~port ~ok:false
        (Error (`generic ("wrong request", Printexc.to_string exn)))
end

module Make(S : S) = struct
  include Lib(S)
  let () = main ()
end

module type SAccount = sig
  include SAccount
  module S : S
  val account : request_source -> (([< account account_aux ], [`generic of (string * string)]) result -> unit) -> unit
  val enable : id:int -> (([< account account_aux ], S.response_error) result -> unit) -> unit
  val unlock : id:int -> (([< account account_aux ], S.response_error) result -> unit) -> unit
  val approve : id:int -> (([< account account_aux ], S.response_error) result -> unit) -> unit

  val handle_request :
    src:request_source -> id:int -> account account_aux -> S.request ->
    ((S.response_ok option, S.response_error) result -> unit) -> unit
end

module MakeAccount(S : SAccount) = struct
  include Lib(S.S)

  let rec handle_account ~id account f =
    let aux = function
      | Error e -> send_res ~id ~ok:false (Error (`custom e))
      | Ok account -> handle_account ~id account f in
    match account with
    | `not_enabled -> S.enable ~id aux
    | `not_approved -> S.approve ~id aux
    | `locked -> S.unlock ~id aux
    | `connected account -> f account

  let () =
    load_config S.S.handle_config;
    Runtime.onConnect @@ fun port ->
    print_conn port;
    Utils.Browser.addListener1 (port##.onDisconnect) (fun p ->
        log "disconnect %s " (to_string p##.name);
        remove_port ~port);
    Utils.Browser.addListener1 (port##.onMessage) @@ fun req ->
    try
      let req = request_aux_of_jsoo S.S.request_jsoo_conv req in
      let id = req.req_id in
      try
        S.account req.req_src (function
            | Error (`generic (name, msg)) ->
              send_res ~id ~ok:false (Error (`generic (name, msg)))
            | Ok account ->
              handle_account ~id:req.req_id account @@ fun account ->
              S.handle_request ~id ~src:req.req_src account req.req_input @@ function
              | Ok None -> ()
              | Ok (Some r) -> send_res ~id (Ok r)
              | Error e -> send_res ~id ~ok:false (Error (`custom e)))
      with exn ->
        send_res ~id ~ok:false
          (Error (`generic ("extension error", Printexc.to_string exn)))
    with exn ->
      send_res ~id:req##.id ~ok:false
        (Error (`generic ("wrong request", Printexc.to_string exn)))

end
