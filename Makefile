all: build

build:
	@dune build --profile release src

clean:
	@dune clean
	@rm -rf _example

deps:
	@opam install . --deps-only

dev:
	@dune build --profile release
	@mkdir -p _example
	@cp -r src/js src/html src/img src/manifest.json _example
	@cp -f _build/default/example/back.bc.js _example/js/background.js
	@cp -f _build/default/example/cli.bc.js _example/js/client.js
	@cp -f example/index.html _example/html/
	@cp -f example/popup.html _example/html/
	@cp -f example/notification.html _example/html/
	@cp -f example/*.js _example/js/
	@mkdir -p _example/css
	@cp -f example/*.css _example/css/
